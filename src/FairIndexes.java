public class FairIndexes {

    // https://leetcode.com/discuss/interview-question/546703/Microsoft-or-OA-2020-or-Count-Visible-Nodes-in-Binary-Tree
    //use long?
    private static int getNumOfFairIndexes(int[] a, int[] b) {
        int[] aa = new int[a.length];
        int[] bb = new int[b.length];
        int sumA = 0;
        int sumB = 0;

        for (int i = 0; i < a.length; i++) {
            sumA += a[i];
            sumB += b[i];
            aa[i] = sumA;
            bb[i] = sumB;
        }
        int count = 0;
        for (int i = 1; i < a.length; i++) {
            int aLeft = aa[i - 1];
            int aRight = sumA - aa[i - 1];
            int bLeft = bb[i - 1];
            int bRight = sumB - bb[i - 1];
            if (aLeft == aRight && aRight == bLeft && bLeft == bRight) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        int[] A1 = {4,-1,0,3}, B1 = {-2, 5, 0 ,3};
        int[] A2 = {2,-2,-3,3}, B2 = {0,0,4,-4};
        int[] A3 = {4,-1,0,3}, B3 = {-2,6,0,4};
        int[] A4 = {3,2,6}, B4 = {4,1,6};
        int[] A5 = {1,4,2,-2,5}, B5 = {7,-2,-2,2,5};
        System.out.println(getNumOfFairIndexes(A1, B1));
        System.out.println(getNumOfFairIndexes(A2, B2));
        System.out.println(getNumOfFairIndexes(A3, B3));
        System.out.println(getNumOfFairIndexes(A4, B4));
        System.out.println(getNumOfFairIndexes(A5, B5));
    }
}
