
//https://leetcode.com/discuss/interview-question/398039/
public class StringWithoutTreeIdenticalLetters {
    private static String reduce(String s) {
        StringBuilder sb = new StringBuilder();

        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (i == 0 || s.charAt(i) == s.charAt(i - 1)) {
                count++;
            } else {
                count = 1;
            }
            if (count < 3) {
                sb.append(s.charAt(i));
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(reduce("eedaaad"));
        System.out.println(reduce("xxxtxxx"));
        System.out.println(reduce("uuuuxaaaaxuuu"));
        System.out.println(reduce("aeeeeeea"));
        System.out.println(reduce("absvsja"));
    }
}
