import java.util.HashMap;
import java.util.Map;

public class ReturnLargestNumberXOccursXTimes {
    //https://leetcode.com/discuss/interview-question/525977/Microsoft-or-OA-2020-or-Return-largest-number-X-which-occurs-X-times

    private static final int MAX = 100000;
    private static int fn(int[] a) {
        Map<Integer, Integer> map = new HashMap<>();
        int max = 0;
        for (int i : a) {
            if (i > MAX) {
                continue;
            }
            map.put(i, map.getOrDefault(i, 0) + 1);
        }

        for (int k : map.keySet()) {
            if (k == map.get(k) && k > max) {
                max = k;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        System.out.println(fn(new int[]{3,8,2,3,3,2})); //3
        System.out.println(fn(new int[]{7,1,2,8,2})); //2
        System.out.println(fn(new int[]{3,1,4,1,5})); //0
        System.out.println(fn(new int[]{5,5,5,5,5})); //5
        System.out.println(fn(new int[]{100002, 100005, 1})); //1
    }
}
