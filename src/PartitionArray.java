import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PartitionArray {

    public static List<List<Integer>> canPartitionKSubsets(int[] nums, int k) {
        List<List<Integer>> re = new ArrayList<List<Integer>>();
        int sum = 0;
        for (int i : nums) {
            sum += i;
        }
        if (sum % k != 0) {
            return re;
        }
        int p = sum / k;
        for (int i : nums) {
            if (i > p) {
                return re;
            }
        }
        int l = nums.length;
        boolean[] vi = new boolean[l];
        Arrays.sort(nums);

        dfs(nums, k, k, p, vi, 0, l - 1, re, new ArrayList<>());
        return re;
    }

    private static void dfs(int[] nums, int n, int k, int p, boolean[] vi, int total, int start, List<List<Integer>> re, List<Integer> cur) {
        if (total > p) {
            return;
        }
        if (k == 0) {
            return;
        }
        if (total == p) {
            re.add(new ArrayList<>(cur));
            dfs(nums, n, k - 1, p, vi, 0, nums.length - 1, re, new ArrayList<>());
            return;
        }
        if (re.size() == n) {
            return;
        }

        for (int i = start; i >= 0; i--) {
            if (!vi[i] && total + nums[i] <= p) {
                vi[i] = true;
                cur.add(nums[i]);
                dfs(nums, n, k, p, vi, total + nums[i], i - 1, re, cur);
                cur.remove(cur.size() - 1);
                vi[i] = false;
            }
        }
    }

    public static void main(String[] args) {
        List<List<Integer>> re = canPartitionKSubsets(new int[]{1, 2, 3, 4, 5}, 3);
        for (List<Integer> l : re) {
            System.out.println("==");
            for (int i : l) {
                System.out.print(i + ",");
            }
            System.out.println("");
        }

    }
}
