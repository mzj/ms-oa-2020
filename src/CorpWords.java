public class CorpWords {

//    https://leetcode.com/discuss/interview-question/507367/Microsoft-or-OA-2020-or-Crop-Words
    private static String corp(String s, int k) {

        char[] c = s.toCharArray();
        int l = c.length;
        if (k > l) {
            return removeTailingZeros(s);
        }
        if (c[k - 1] == ' ') {
            return removeTailingZeros(s.substring(0, k));
        }
        if (k - 1 == l - 1 || (k < l && c[k] == ' ')) {
            return s.substring(0, k);
        }

        while (k >= 1 && c[k - 1] != ' ') {
            k--;
        }
        return removeTailingZeros(s.substring(0, k));
    }

    private static String removeTailingZeros(String s) {
        StringBuilder sb = new StringBuilder(s);
        for (int i = sb.length() - 1; i >= 0; i--) {
            if (sb.charAt(i) == ' ') {
                sb.deleteCharAt(i);
            } else {
                break;
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(corp(" co", 3)); //" c"
        // System.out.println(solve(" co", 3)); //" c"
        System.out.println(corp("Word", 4)); //"Word"
        System.out.println(corp(" co de my", 5)); //" co"
        System.out.println(corp("   ", 2)); //""
        System.out.println(corp("   re", 3)); //""
        System.out.println(corp("Codility Me test coders", 1)); //""
        System.out.println(corp("Codility  Me  test  coders", 20)); //Codility Me
        System.out.println(corp("Codility Me test coders", 100)); //all
        System.out.println(corp("Codility Me test coders   ", 100)); //all
        System.out.println(corp("Codility Me test coders", 8)); //Codility
        System.out.println(corp("Codility Me test coders", 9)); //Codility
        System.out.println(corp("Codility Me test coders", 10)); //Codility
        System.out.println(corp("Codility Me test coders", 11)); //Codility Me
        System.out.println(corp("Codility Me test coders", 12)); //Codility Me

    }
}
