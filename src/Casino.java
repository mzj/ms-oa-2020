public class Casino {

    private static int casino(int n, int k) {
        return fn(n, k, 0);
    }

    private static int fn(int n, int k, int total) {
        if (k == 0) {
            return total + n - 1;
        }

        if (n % 2 == 0) {
            return fn(n / 2, k - 1, total + 1);
        }
        return fn(n - 1, k, total + 1);
    }

    private static int getMinRounds(int n, int k) {
        int[][] dp = new int[k+1][n+1];
        for(int i=0;i<=k;i++) {
            for(int j=1;j<=n;j++) {
                if(i == 0) {
                    if(j == 1)
                        dp[i][j] = 0;
                    else
                        dp[i][j] = dp[i][j-1] + 1;
                }else {
                    if(j%2 == 0)
                        dp[i][j] = Math.min(dp[i-1][j], dp[i-1][j/2] + 1);
                    else
                        dp[i][j] = dp[i][j-1] + 1;
                }
            }
        }
        return dp[k][n];
    }

    public static void main(String[] args) {
        System.out.println(casino(18,1)); //9
        System.out.println(casino(18,2)); //6
        System.out.println(casino(18,3)); //5
        System.out.println(casino(18,4)); //5
        System.out.println(casino(1,0)); //7
        System.out.println(casino(5,0)); //7
        System.out.println(casino(8,0)); //7

        System.out.println(getMinRounds(18,1)); //9
        System.out.println(getMinRounds(18,2)); //6
        System.out.println(getMinRounds(18,3)); //5
        System.out.println(getMinRounds(18,4)); //5
        System.out.println(getMinRounds(1,0)); //7
        System.out.println(getMinRounds(5,0)); //7
        System.out.println(getMinRounds(8,0)); //7

    }
}
