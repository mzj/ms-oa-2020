public class MaxChunckToSortArray {

    //Max Chunks to Sort Array
    // https://leetcode.com/discuss/interview-question/524146/Microsoft-or-OA-or-Max-Chunks-to-Sort-Array

    private static int fn(int[] a) {

        return 1;
    }

    public static void main(String[] args) {
        int[] nums1 = {2,4,1,6,5,9,7};
        int[] nums2 = {4,3,2,6,1};
        int[] nums3 = {2,1,6,4,3,7};
        System.out.println(fn(nums1));
        System.out.println(fn(nums2));
        System.out.println(fn(nums3));
    }
}
