import java.util.Arrays;

public class IncreaseThenDecrease {
    //[10,9,2,5,3,7,101,18]
    public static int lengthOfLIS(int[] nums) {
        int l = nums.length;
        int[] incr = new int[l];
        Arrays.fill(incr, 1);
        System.out.println("incr");
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i]) {
                    incr[i] = Math.max(incr[i], incr[j] + 1);
                }
            }

            System.out.println(incr[i]);
        }
        int[] desc = new int[l];
        Arrays.fill(desc, 1);
        System.out.println("desc");
        for (int i = l - 1; i >= 0; i--) {
            for (int j = l - 1; j > i; j--) {
                if (nums[i] > nums[j]) {
                    desc[i] = Math.max(desc[i], desc[j] + 1);
                }

            }
            System.out.println(desc[i]);
        }
        int maxL = 0;
        for (int i = 0; i < l; i++) {
            maxL = Math.max(maxL, incr[i] + desc[i] - 1);
        }
        System.out.println("over");
        return l - maxL;
    }
    public static void main(String[] args) {
        System.out.println(lengthOfLIS(new int[]{10,9,2,5,3,7,101,18})); //1
        System.out.println(lengthOfLIS(new int[]{2,3,15,5,7,6,4,1}));
    }
}
