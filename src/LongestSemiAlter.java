public class LongestSemiAlter {
    //https://leetcode.com/discuss/interview-question/398037/
    private static int solve(String s) {

        int l = s.length();
        int count = 0;
        int start = 0;
        int max = 0;
        for (int i = 0; i < l; i++) {
            if (i == 0 || s.charAt(i) == s.charAt(i - 1)) {
                count++;
            } else {
                count = 1;
            }

            if (count <= 2) {
                max = Math.max(max, i - start + 1);
            } else {
                count = 2;
                start = i - 1;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        System.out.println(solve("baaabbabbb"));
        System.out.println(solve("abaaaaaabbaabbaabb")); //12
        System.out.println(solve("babba"));
        System.out.println(solve("a"));
        System.out.println(solve("aabbaabbaabbaabbbaabb"));

    }
}
