public class MaxPossibleValue {

    //Max Possible Value
    //https://leetcode.com/discuss/interview-question/398050/
    //insert 5
    private static int insert(int n) {
        StringBuilder sb = new StringBuilder(String.valueOf(Math.abs(n)));

        if (n >= 0) {
            int i = 0;
            while (i < sb.length()) {
                if (sb.charAt(i) - '0' < 5) {
                    break;
                }
                i++;
            }
            sb.insert(i, 5);
        } else {
            int i = 0;
            while (i < sb.length()) {
                if (sb.charAt(i) - '0' > 5) {
                    break;
                }
                i++;
            }
            sb.insert(i, 5);
        }
        //
        return Integer.valueOf(sb.toString()) * (n >= 0 ? 1 : -1);
    }
    public static void main(String[] args) {
        System.out.println(insert(268)); //5268
        System.out.println(insert(945)); //9545
        System.out.println(insert(670)); //6750
        System.out.println(insert(999)); //9995
        System.out.println(insert(0));   //50
        System.out.println(insert(-1));  //-15
        System.out.println(insert(-9));  //-59
        System.out.println(insert(-123)); //-1235
        System.out.println(insert(-1253)); //-12535
        System.out.println(insert(-256)); //-2556
        System.out.println(insert(-268)); //-2568
        System.out.println(insert(-999)); //-5999
        System.out.println(insert(-945)); //-5945
        System.out.println(insert(-439)); //-4359
    }
}
